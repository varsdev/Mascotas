﻿using Mascotas.BO;
using Mascotas.DAO;
using Mascotas.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mascotas
{
    public partial class FrmPrincipal : Form
    {
        DateTimePicker dtpFechaIngreso = new DateTimePicker();
        Rectangle rectangle;
        Mascota masc;
        Mascota mascPrin;
        Duenno dC;

        public FrmPrincipal()
        {
            InitializeComponent();
            CenterToScreen();

            dgvRegMas.Controls.Add(dtpFechaIngreso);
            dtpFechaIngreso.Visible = false;
            dtpFechaIngreso.Format = DateTimePickerFormat.Custom;
            dtpFechaIngreso.TextChanged += new EventHandler(dtp_TextChange);
            mostrarAdoptar();
            mostrarPrin();

        }


        public void mostrarPrin()
        {
            bool sex;
            dgvRegMas.Rows.Clear();
            foreach (Mascota m in new MascotaBO().CargarMascotas())
            {
                if (m.estado == true)
                {
                    if (m.sexo == 'M')
                    {
                        sex = true;
                        dgvRegMas.Rows.Add(m, m.id, m.color, m.tamanoo, sex, m.edad, m.fechaIngreso.ToShortDateString(), m.foto);
                    }
                    if (m.sexo == 'F')
                    {
                        sex = false;
                        dgvRegMas.Rows.Add(m, m.id, m.color, m.tamanoo, sex, m.edad, m.fechaIngreso.ToShortDateString(), m.foto);

                    }
                }
            }
        }

        public void mostrarAdoptar()
        {
            string sex;
            dgvAdopcion.Rows.Clear();
            foreach (Mascota m in new MascotaBO().CargarMascotas())
            {
                if (m.estado == true)
                {
                    if (m.sexo == 'M')
                    {

                        sex = "Macho";
                        dgvAdopcion.Rows.Add(m, m.id, m.color, m.tamanoo, sex, m.edad, m.fechaIngreso.ToShortDateString(), m.foto);
                    }
                    if (m.sexo == 'F')
                    {
                        sex = "Hembra";
                        dgvAdopcion.Rows.Add(m, m.id, m.color, m.tamanoo, sex, m.edad, m.fechaIngreso.ToShortDateString(), m.foto);

                    }
                }
            }
        }

        public void mostrarRep(string cedula)
        {
            dgvReporte.Rows.Clear();
            foreach (var item in new AdopcionBO().CargarAdopciones(cedula))
            {
                dgvReporte.Rows.Add(item);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                mascPrin = (Mascota)dgvRegMas.Rows[e.RowIndex].Cells[0].Value;
            }
            catch (Exception)
            {

            }

            try
            {

                switch (dgvRegMas.Columns[e.ColumnIndex].Name)
                {
                    case "Fechaingreso":
                        rectangle = dgvRegMas.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                        dtpFechaIngreso.Size = new Size(rectangle.Width, rectangle.Height);
                        dtpFechaIngreso.Location = new Point(rectangle.X, rectangle.Y);
                        dtpFechaIngreso.Visible = true;
                        break;

                    case "Foto":
                        try
                        {
                            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                            {
                                string imagen = openFileDialog1.FileName;
                                dgvRegMas.CurrentRow.Cells[7].Value = Image.FromFile(imagen);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("El archivo seleccionado no es un tipo de imagen válido");
                        }

                        break;

                    case "Registro":
                        try
                        {
                            if (mascPrin != null)
                            {
                                mascPrin.color = dgvRegMas.CurrentRow.Cells[2].Value.ToString();
                                mascPrin.tamanoo = dgvRegMas.CurrentRow.Cells[3].Value.ToString();
                                if ((bool)dgvRegMas.Rows[e.RowIndex].Cells[4].Value == true)
                                {
                                    mascPrin.sexo = 'M';
                                }
                                else if ((bool)dgvRegMas.Rows[e.RowIndex].Cells[4].Value == false)
                                {
                                    mascPrin.sexo = 'F';
                                }
                                mascPrin.edad = double.Parse(dgvRegMas.Rows[e.RowIndex].Cells[5].Value.ToString());
                                mascPrin.fechaIngreso = DateTime.Parse(dgvRegMas.CurrentRow.Cells[6].Value.ToString());
                                mascPrin.foto = (Image)dgvRegMas.CurrentRow.Cells[7].Value;
                                new MascotaBO().Insertar(mascPrin);
                                mostrarAdoptar();
                                MessageBox.Show("Mascota editada");
                            }
                            else
                            {
                                Mascota m = new Mascota();
                                if (dgvRegMas.CurrentRow.Cells[2].Value == null)
                                {
                                    MessageBox.Show("Debe seleccionar un color");
                                }
                                else
                                {
                                    m.color = dgvRegMas.CurrentRow.Cells[2].Value.ToString();
                                }

                                if (dgvRegMas.CurrentRow.Cells[3].Value == null)
                                {
                                    MessageBox.Show("Debe seleccionar un tamaño");
                                }
                                else
                                {
                                    m.tamanoo = dgvRegMas.CurrentRow.Cells[3].Value.ToString();
                                }

                                if (dgvRegMas.Rows[e.RowIndex].Cells[4].Value == null)
                                {
                                    m.sexo = 'F';
                                }
                                else
                                {
                                    if ((bool)dgvRegMas.Rows[e.RowIndex].Cells[4].Value == true)
                                    {
                                        m.sexo = 'M';
                                    }
                                    else if ((bool)dgvRegMas.Rows[e.RowIndex].Cells[4].Value == false)
                                    {
                                        m.sexo = 'F';
                                    }
                                }

                                if (dgvRegMas.Rows[e.RowIndex].Cells[5].Value == null)
                                {
                                    MessageBox.Show("Debe digitar la edad");
                                }
                                else
                                {
                                    m.edad = double.Parse(dgvRegMas.Rows[e.RowIndex].Cells[5].Value.ToString());
                                }

                                m.foto = (Image)dgvRegMas.CurrentRow.Cells[7].Value;

                                if (dgvRegMas.CurrentRow.Cells[6].Value == null)
                                {
                                    MessageBox.Show("Debe seleccionar un fecha");
                                }
                                else
                                {
                                    m.fechaIngreso = DateTime.Parse(dgvRegMas.CurrentRow.Cells[6].Value.ToString());
                                    m.estado = true;
                                    int id = new MascotaBO().Insertar(m);
                                    m.id = id;
                                    dgvRegMas.CurrentRow.Cells[1].Value = id;
                                    mostrarPrin();
                                    mostrarAdoptar();
                                    MessageBox.Show("Mascota registrada");
                                }

                            }

                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Intente nuevamente");
                        }
                        break;

                    case "Eliminar":
                        try
                        {
                            if (mascPrin != null)
                            {
                                new MascotaBO().Eliminar(mascPrin);
                                MessageBox.Show("Mascota eliminada");
                                mostrarPrin();
                                mostrarAdoptar();
                            }
                        }
                        catch (Exception)
                        {

                            MessageBox.Show("Intente nuevamente");
                        }
                        break;
                }

            }
            catch (Exception)
            {

            }


        }

        private void dtp_TextChange(object sender, EventArgs e)
        {
            dgvRegMas.CurrentCell.Value = dtpFechaIngreso.Text.ToString();
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            dtpFechaIngreso.Visible = false;
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dtpFechaIngreso.Visible = false;
        }

        private void dgvAdopcion_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtNombreAdop.Enabled = true;
                btnAdoptar.Enabled = true;
                masc = (Mascota)dgvAdopcion.Rows[e.RowIndex].Cells[0].Value;

            }
            catch (Exception)
            {

            }
        }

        private void btnConsultarAdop_Click(object sender, EventArgs e)
        {
            try
            {
                DuennoDAO ddao = new DuennoDAO();
                dC = ddao.ConsultarDatos(txtCedulaAdop.Text.Trim());
                if (dC != null)
                {
                    txtNombreAdop.Text = dC.nombre;
                    btnAdoptar.Enabled = true;
                    MessageBox.Show("Persona encontrada");
                }
                else
                {
                    MessageBox.Show("Persona no registrada");
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnAdoptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (masc != null)
                {
                    Adopcion a = new Adopcion();
                    if (dC == null)
                    {
                        Duenno d = new Duenno();
                        d.cedula = txtCedulaAdop.Text.Trim();
                        d.nombre = txtNombreAdop.Text.Trim();
                        int id = new DuennoBO().Insertar(d);
                        d.id = id;
                        a.duenno = d;
                    }
                    else
                    {
                        a.duenno = dC;
                    }

                    a.mascota = masc;
                    DateTime fecha = DateTime.Now;
                    a.fechaAdopcion = fecha.Date;
                    new AdopcionBO().Insertar(a);

                    bool estado = false;
                    new MascotaBO().Estado(masc, estado);
                    mostrarAdoptar();
                    mostrarPrin();
                    MessageBox.Show("Adopcion registrada");
                    txtCedulaAdop.ResetText();
                    txtNombreAdop.ResetText();
                    txtNombreAdop.Enabled = false;
                    btnAdoptar.Enabled = false;

                }

            }
            catch (Exception)
            {
                MessageBox.Show("Intente nuevamente");
            }
        }

        private void btnBuscarRep_Click(object sender, EventArgs e)
        {
            try
            {
                mostrarRep(txtCedulaRep.Text.Trim());
                txtCedulaRep.ResetText();
            }
            catch (Exception)
            {
                MessageBox.Show("Intente nuevamente");
            }

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
