﻿using Mascotas.DAO;
using Mascotas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mascotas.BO
{
    class MascotaBO
    {
        public int Insertar(Mascota m)
        {
            if (m != null)
            {
                if (m.id > 0)
                {
                    new MascotaDAO().ModificarDatos(m);
                    return 0;
                }
                else
                {
                    return new MascotaDAO().InsertarDatos(m);

                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Debe registrar una mascota");
                return 0;
            }       
        }

        public void Estado(Mascota m, bool estado)
        {
            if (m != null)
            {
                new MascotaDAO().Estado(m, estado);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Debe seleccionar una mascota");
            }
        }

        public List<Mascota> CargarMascotas()
        {
            return new MascotaDAO().ConsultarDatos();
        }

        public void Eliminar(Mascota m)
        {
            if (m != null)
            {
                new MascotaDAO().Eliminar(m.id);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Debe seleccionar una mascota");
            }
        }
    }
}
