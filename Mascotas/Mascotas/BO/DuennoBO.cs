﻿using Mascotas.DAO;
using Mascotas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mascotas.BO
{
    class DuennoBO
    {
        public int Insertar(Duenno d)
        {
            if (d != null)
            {
                return new DuennoDAO().InsertarDatos(d);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Debe registar una persona");
                return 0;
            }           
        }

        public Duenno CargarDuenno(string filtro)
        {
            return new DuennoDAO().ConsultarDatos(filtro);
        }
    }
}
