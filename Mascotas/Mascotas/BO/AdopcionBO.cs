﻿using Mascotas.DAO;
using Mascotas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mascotas.BO
{
    class AdopcionBO
    {
        public void Insertar(Adopcion a)
        {
            if (a != null)
            {
                 new AdopcionDAO().InsertarDatos(a);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Debe registrar una Adopcion");
            }
        }

        public List<object> CargarAdopciones(string filtro)
        {
            return new AdopcionDAO().Reporte(filtro);
        }
    }
}
