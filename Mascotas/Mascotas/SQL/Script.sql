CREATE DATABASE adopta;

CREATE TABLE mascotas
(
	id serial primary key,
	color text not null,
	tamanno text not null,
	sexo char default 'M',
	edad double precision not null,
	fecha_ingreso date not null,
	foto bytea not null,
	estado boolean default true	
);

CREATE TABLE duennos
(
	id serial primary key,
	cedula text not null,
	nombre text not null,
	CONSTRAINT unq_duennos_cedula UNIQUE (cedula)
);

CREATE TABLE adopciones
(
	id serial primary key,
	id_duenno int not null,
	id_mascota int not null,
	fecha_adopcion date not null,
	FOREIGN KEY(id_duenno) references duennos(id),
	FOREIGN KEY(id_mascota) references mascotas(id)
);

--Consulta
SELECT d.nombre, m.color, m.tamanno, m.sexo, m.edad, a.fecha_adopcion 
FROM duennos as d 
JOIN adopciones as a ON d.id = a.id_duenno 
JOIN mascotas as m ON m.id = a.id_mascota WHERE d.cedula = '207860816' 

drop table mascotas
drop table adopciones

delete from mascotas;
delete from adopciones;
delete from duennos;

Select * from mascotas;
select * from duennos;
select * from adopciones;
