﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mascotas.Entities
{
    class Duenno
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }

        public override string ToString()
        {
            return cedula + nombre; 
        }
    }
}
