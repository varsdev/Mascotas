﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mascotas.Entities
{
    class Adopcion
    {
        public Duenno duenno { get; set; }
        public Mascota mascota { get; set; }
        public DateTime fechaAdopcion { get; set; }

        public override string ToString()
        {
            return "" + duenno + mascota + fechaAdopcion;
        }

    }
}
