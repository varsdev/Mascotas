﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mascotas.Entities
{
    class Mascota
    {
        public int id { get; set; }
        public string color { get; set; }
        public string tamanoo { get; set; }
        public char sexo { get; set; }
        public double edad { get; set; }
        public DateTime fechaIngreso { get; set; }
        public Image foto { get; set; }
        public bool estado { get; set; }

        public override string ToString()
        {
            return id + color + tamanoo + sexo + edad + fechaIngreso + estado + foto; 
        }

    }
}
