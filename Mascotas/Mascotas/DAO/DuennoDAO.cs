﻿using Mascotas.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mascotas.DAO
{
    class DuennoDAO
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        public int InsertarDatos(Duenno d)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO duennos (cedula, nombre) VALUES ('" + d.cedula + "', '" + d.nombre + "') returning id; ", conexion);
            int id = (int)cmd.ExecuteScalar();
            conexion.Close();
            return id;
        }

        public Duenno ConsultarDatos(string cedula)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre FROM duennos WHERE cedula = '" + cedula + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    return Cargar(dr);
                }
            }
            conexion.Close();

            return null;
        }

        private Duenno Cargar(NpgsqlDataReader dr)
        {
            Duenno d = new Duenno();
            d.id = dr.GetInt32(0);
            d.cedula = dr.GetString(1);
            d.nombre = dr.GetString(2);

            return d;
        }

    }
}
