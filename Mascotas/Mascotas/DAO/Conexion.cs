﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Mascotas.DAO
{
    class Conexion
    {
        static NpgsqlConnection conexion;
        /// <summary>
        /// Conecta con la base de datos
        /// </summary>
        /// <returns></returns> retorna la conexion
        public static NpgsqlConnection ConexionS()
        {
            string servidor = "localhost";
            int puerto = 5432;
            string usuario = "postgres";
            string clave = "postgres";
            string baseDatos = "adopta";

            string cadenaConexion = "Server=" + servidor + ";" + "Port=" + puerto + ";" + "User Id=" + usuario + ";" + "Password=" + clave + ";" + "Database=" + baseDatos;
            return conexion = new NpgsqlConnection(cadenaConexion);
        }
    }
}
