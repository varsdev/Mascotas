﻿using Mascotas.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mascotas.DAO
{
    class MascotaDAO
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        public int InsertarDatos(Mascota m)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO mascotas (color, tamanno, sexo, edad, fecha_ingreso, foto, estado) VALUES ('" + m.color + "', '" + m.tamanoo + "', '" + m.sexo + "', '" + m.edad + "', '" + m.fechaIngreso + "', '" + ImagenByte(m.foto) + "', '" + m.estado + "') returning id; ", conexion);
            int id = (int)cmd.ExecuteScalar();
            conexion.Close();
            return id;
        }

        public void ModificarDatos(Mascota m)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("UPDATE mascotas SET color = '" + m.color + "', tamanno = '" + m.tamanoo + "', sexo = '" + m.sexo + "', edad = '" + m.edad + "', fecha_ingreso = '" + m.fechaIngreso + "', foto = '" + ImagenByte(m.foto) + "'   WHERE id = '" + m.id + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public void Estado(Mascota m, bool estado)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("UPDATE mascotas SET estado = '" + estado + "' WHERE id = '" + m.id + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public void Eliminar(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("DELETE FROM mascotas WHERE id = '" + id + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public List<Mascota> ConsultarDatos()
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Mascota> lista = new List<Mascota>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, color, tamanno, sexo, edad, fecha_ingreso, foto, estado FROM mascotas Order by id", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();

            return lista;
        }

        private Mascota Cargar(NpgsqlDataReader dr)
        {
            byte[] bytes = dr["foto"] as byte[] ?? null;
            Mascota m = new Mascota()
            {
                id = dr.GetInt32(0),
                color = dr.GetString(1),
                tamanoo = dr.GetString(2),
                sexo = dr.GetChar(3),
                edad = dr.GetDouble(4),
                fechaIngreso = dr.GetDateTime(5),
                foto = ByteImage(bytes),
                estado = dr.GetBoolean(7)

            };
            return m;
        }

        /*
        public byte[] ImBy(System.Drawing.Image image)
        {
            using(var ms = new MemoryStream())
            {
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);

                return ms.ToArray();
            }
        }
        */
        
        public byte[] ImagenByte(Image pImagen)
        {
            byte[] mImage;
            try
            {
                if (pImagen != null)
                {
                    using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                    {
                        pImagen.Save(ms, pImagen.RawFormat);
                        mImage = ms.GetBuffer();
                        ms.Close();
                    }
                }
                else { mImage = null; }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return mImage;
        }
        

        //System.IO.MemoryStream
        //System.Drawing.dll

        private Image ByteImage(byte[] bytes)
        {
           if (bytes == null) return null;
            MemoryStream ms = new MemoryStream(bytes);

            
            Bitmap bm = null;
            try
            {
                bm = new Bitmap(ms);//aqui se cae
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return bm;

        }
    }
}
