﻿using Mascotas.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mascotas.DAO
{
    class AdopcionDAO
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        public void InsertarDatos(Adopcion a)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO adopciones (id_duenno, id_mascota, fecha_adopcion) VALUES ('" + a.duenno.id + "', '" + a.mascota.id + "', '" + a.fechaAdopcion + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public List<object> Reporte(string cedula)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<object> lista = new List<object>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT d.nombre, m.color, m.tamanno, m.sexo, m.edad, a.fecha_adopcion FROM duennos as d JOIN adopciones as a ON d.id = a.id_duenno JOIN mascotas as m ON m.id = a.id_mascota WHERE d.cedula = '" + cedula + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(MascotasAdoptadas(dr));
                }
            }
            conexion.Close();

            return lista;
        }


        private String MascotasAdoptadas(NpgsqlDataReader dr)
        {
            string consulta = "";
            consulta += "Dueño: " + dr.GetString(0);
            consulta += "  Color: " + dr.GetString(1);
            consulta += "  Tamaño: " + dr.GetString(2);
            if (dr.GetChar(3) == 'M')
            {
                consulta += "  Sexo: Macho";
            }
            else if (dr.GetChar(3) == 'F')
            {
                consulta += "  Sexo: Hembra";

            }
            consulta += "  Edad: " + dr.GetDouble(4);
            consulta += "  Fecha de adopcion: " + dr.GetDateTime(5).ToShortDateString();

            return consulta;
        }



    }
}
